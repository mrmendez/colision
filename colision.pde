
Circulo c1,c2 ;
float dx,dy;
boolean ban= false;
String mensaje;

void setup(){
   size(300,300);
   c1 = new Circulo(width/2, height/2,60);
   c2 = new Circulo(width/2-80, height-80,40);
   mensaje = "AQUI VA EL DATO DE COLISION";
}

void draw(){
    background(127);
    c1.dibujar();
    c2.dibujar();
    text(mensaje, 10,15);
}

void mousePressed(){
    if(  c1.isAdentro(mouseX,mouseY)){
        ban = true;
    } else{
       ban = false;
    }
    
    dx = mouseX - c1.x;
    dy = mouseY - c1.y;
}

void mouseDragged(){
  
    if( ban ){
    c1.x = mouseX-dx;
    c1.y = mouseY-dy;
    }
    
    if (  c1.isColision(c2)   ){
       mensaje = "COLISION ";
    }else{
        mensaje = "NO HUBO NADA";
    }
    
}