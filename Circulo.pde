
class Circulo{

     float x;
     float y;
     float radio;
     
     public Circulo(float a, float b , float c){
         this.x = a;
         y = b;
         radio = c;
         ellipseMode(CENTER);
     }
     
     public void dibujar(){
        ellipse(x,y, 2*radio, 2*radio);
     }
     
     public boolean isAdentro(float a, float b){
         float distancia = dist(x,y,a,b);
         float e = distancia -radio;
         if (e <= 0){
             return true;
         }else{
             return false;
         }
     }
     
 public boolean isColision(Circulo a){
       float l = dist(x,y, a.x, a.y);
       float d = l - radio - a.radio;
       if (d <= 0){
           return true;
       }
       else{
          return false;
       }
 }    
        
}